From Coq Require Import List.
From Equations Require Import Equations.
Set Implicit Arguments.

Inductive In {A} (x : A) : list A -> Type :=
| here    xs : In x (x :: xs)
| there y xs : In x xs -> In x (y :: xs).
Arguments here {_ _ _}.
Arguments there {_ _ _ _}.
Infix "∈" := In (at level 10).

Inductive All {A} (P : A -> Type) : list A -> Type :=
| AllNil  : All P nil
| AllCons x xs : P x -> All P xs -> All P (x :: xs).

Equations lookup {A} (l : list A) P x
  (IN : x ∈ l) (γ : All P l) : P x :=
    lookup here       (AllCons _ v _)   := v;
    lookup (there In) (AllCons _ _ rec) := lookup In rec.

Definition push {A P} {l : list A} {x} (v : P x) (γ : All P l) : All P (x :: l) :=
  AllCons _ v γ.
Infix "▷" := push (at level 10).
