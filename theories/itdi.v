(*|
Intrinsically Typed Definitional Interpreters
=============================================

Implementing in Coq interpreters from the paper
"Intrinsically-Typed Definitional Interpreters for Imperative Languages"
by Poulsen et al.

|*)
Set Implicit Arguments.
From Coq Require Import List.
Import ListNotations.
From ITDI Require Import utils.
From ITree Require Import ITree.
From Equations Require Import Equations.
From ExtLib Require Import
     Structures.Functor
     Structures.Monad
     Data.Monads.OptionMonad.
Import Monad.
Import Monads.
Import MonadNotation.
Open Scope monad.

(*|
A simple SN expression language
-------------------------------
|*)

Module Expr.

  (* Static *)
  Variant typ : Set := Nat | Bool.
  Derive NoConfusion for typ.

  Definition ctx := list typ.

  Inductive expr {Γ : ctx} : typ -> Type :=
  | BE   : bool -> expr Bool
  | NE   : nat -> expr Nat
  | VarE : forall τ, τ ∈ Γ -> expr τ
  | AddE : expr Nat -> expr Nat -> expr Nat
  | IfE  : forall τ, expr Bool -> expr τ -> expr τ -> expr τ
  .
  Arguments expr : clear implicits.

  (* Dynamic *)
  Variant val : typ -> Set :=
    | BV : bool -> val Bool
    | NV : nat -> val Nat.

  Definition env (Γ : ctx) : Type :=
    All val Γ.

  Equations eval Γ τ (γ : env Γ) : expr Γ τ -> val τ :=
    eval _ (BE b)       := BV b ;
    eval _ (NE n)       := NV n ;
    eval _ (VarE x)     := lookup x γ ;
    eval _ (AddE e1 e2) :=
      let '(NV n1) := eval γ e1 in
      let '(NV n2) := eval γ e2 in
      NV (n1 + n2);
    eval _ (IfE e1 e2 e3) :=
      let '(BV b) := eval γ e1 in
      if b
      then eval γ e2
      else eval γ e3
  .

  (* Equations eval Γ τ (γ : env Γ) : expr Γ τ -> val τ := *)
  (*   eval _ (BE b)       := BV b ; *)
  (*   eval _ (NE n)       := NV n ; *)
  (*   eval _ (VarE x)     := lookup x γ ; *)
  (*   eval _ (AddE e1 e2) := *)
  (*     let '(NV n1) := eval γ e1 in *)
  (*     let '(NV n2) := eval γ e2 in *)
  (*     NV (n1 + n2); *)
  (*   eval _ (IfE e1 e2 e3) with (eval γ e2, eval γ e3) => { *)
  (*     eval _ _ (v1,v2) := *)
  (*         let '(BV b) := eval γ e1 in *)
  (*         if b then v1 else v2 *)
  (* }. *)


End Expr.

Module STLC.

  (* Static *)
  Inductive typ : Set := Unit | Arr (τ1 τ2 : typ).
  Derive NoConfusion for typ.
  Definition ctx := list typ.

  Inductive term : ctx -> typ -> Type :=
  | unit Γ : term Γ Unit
  | var Γ τ : τ ∈ Γ -> term Γ τ
  | abs Γ τ1 τ2 (t : term (τ1 :: Γ) τ2) : term Γ (Arr τ1 τ2)
  | app Γ τ1 τ2 (t : term Γ (Arr τ1 τ2)) (u : term Γ τ1) : term Γ τ2.
  Arguments unit {_}.

  (* dynamic *)
  Inductive val : typ -> Type :=
  | unitV : val Unit
  | absV Γ τ1 τ2 (t : term (τ1 :: Γ) τ2) (γ : All val Γ) : val (Arr τ1 τ2).
  Notation env := (All val).

  Definition M (Γ : ctx) := fun A => env Γ -> option A.
  #[local] Instance M_monad Γ : Monad (M Γ) :=
    {|
      ret := fun _ x _ => Some x ;
      bind := fun _ _ t k γ => match t γ with | None => None | Some x => k x γ end
    |}.

  Definition timeout {Γ : ctx} {A} : M Γ A := fun _ => None.
  Definition getEnv {Γ : ctx} : M Γ (env Γ) := fun γ => ret γ.
  Definition usingEnv {Γ Γ' : ctx} {A} γ (t : M Γ A) : M Γ' A :=
    fun _ => t γ.

  Equations eval (fuel : nat) Γ τ : term Γ τ -> M Γ (val τ) :=
    eval 0     _         := timeout ;
    eval (S _) unit      := ret unitV ;
    eval (S _) (var x)   := γ <- getEnv;; ret (lookup x γ);
    eval (S _) (abs t)   := γ <- getEnv;; ret (absV t γ);
    eval (S k) (app t u) :=
      eval k t >>=
        (λ{ | absV t' γ' :=
              (v <- eval k u;; usingEnv (v ▷ γ') (eval k t'))
        })
  .

End STLC.
