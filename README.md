# ITDI

Coq Implementation of Intrinsically Typed Definitional Interpreter in the style of Poulsen et al.

## Meta

- Author(s):
  - Yannick Zakowski
- License: MIT License
- Compatible Coq versions: 8.15
- Additional dependencies:
  - dune
  - [Extlib](https://github.com/coq-community/coq-ext-lib)
  - [InteractionTrees](https://github.com/DeepSpec/InteractionTrees)
  - [Equations](https://github.com/mattam82/Coq-Equations)
- Coq namespace: `ITDI`

## Building instructions

### Installing dependencies

Installing the opam dependencies
```shell
opam install dune
opam install coq-ext-lib
opam install coq-itree
opam install coq-equations
```

### Obtaining the project

```shell
git clone https://github.com/vellvm/ctrees
cd ctrees
```

### Building the project

```shell
dune build
```

